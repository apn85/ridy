$(document ).ready(function() {

    // Listen to tab events to enable outlines (accessibility improvement)
    document.body.addEventListener('keyup', function(e) {
      if (e.which === 9) /* tab */ {
        document.documentElement.classList.remove('no-focus-outline');
      }
    });

    $('.hamburger').click(function(){
      $(this).toggleClass('is-active');
      $('.mobile_nav').toggleClass('visible');
    });

    $('.faq__question a').click(function(e){

        e.preventDefault();

        var $this = $(this).parent().find('p');
        $(".faq__question--content").not($this).slideUp();
        $(".faq__circle").not($this).toggleClass('active');

        $this.delay(215).slideToggle();

    });

});
