<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/png">
    <title>Ridy - Ride Around Town</title>

    <!-- css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/styles/styles.css" type="text/css" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.1.3/hamburgers.css">
    <!-- /css -->

    <?php wp_head();?>
    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/scripts/scripts.js"></script>
    <!-- /js -->

    <meta name="description" content="Page Content">
</head>

<body class="no-focus-outline">

    <div class="pt2 pb2 show--lg">
        <nav class="grid-container contained">
            <div class="row align--middle">
                <div class="col c10">
                    <ul class="list list--horizontal gutter--large align--middle">
                        <li class="list__item">
                            <a href="/">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="" width="150px" class="db">
                            </a>
                        </li>
                        <li class="list__item">
                            <a href="/how-it-works" class="bold">
                                How It Works
                            </a>
                        </li>
                        <li class="list__item">
                            <a href="/faq" class="bold">
                                FAQ
                            </a>
                        </li>
                        <li class="list__item">
                            <a href="/support" class="bold">
                                Support
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col c6">
                    <div class="tr">
                        <a href="https://play.google.com/store/apps/details?id=com.joyride.ridy&hl=en_US" class="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/google-badge.png" alt="" width="145">
                        </a>
                        <a href="https://itunes.apple.com/US/app/id1450596377?mt=8" class="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/apple-badge.png" alt="" width="130px">
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!-- mobile btn -->
    <div class="mobile_nav--wrapper hide--lg">
        <header class="media align--middle pl1 pr1 pt1">
            <div class="media__fluid">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="" width="150px">
                </a>
            </div>
            <div class="media__fixed">
                <button class="hamburger hamburger--collapse" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </button>
            </div>
        </header>

        <div class="mobile_nav pt12">
            <ul class="list list--vertical gutter--xlarge align--middle">
                <li class="list__item">
                    <a href="/how-it-works" class="bold inter--larger db tc">
                        How It Works
                    </a>
                </li>
                <li class="list__item">
                    <a href="/faq" class="bold inter--larger db tc">
                        FAQ
                    </a>
                </li>
                <li class="list__item">
                    <a href="/support" class="bold inter--larger db tc">
                        Support
                    </a>
                </li>
                <li class="list__item">
                    <div class="tc">
                        <a href="https://play.google.com/store/apps/details?id=com.joyride.ridy&hl=en_US" class="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/google-badge.png" alt="" width="180px">
                        </a>
                        <a href="https://itunes.apple.com/US/app/id1450596377?mt=8" class="">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/apple-badge.png" alt="" width="160px">
                        </a>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <!-- /mobile btn -->



    <div class="wrapper mt8 mt0--lg">
