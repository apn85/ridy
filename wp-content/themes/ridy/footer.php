    </div>

    <footer class="footer bt--orange pt4 pb4 pt2--lg pb2--lg mb10 mb0--lg">
        <div class="grid-container contained">
            <ul class="footer__nav--desktop show--lg">
                <li>
                    <span class="inter--small">© 2019 Ridy Corp. All Rights Reserved.</span>
                </li>
                <li>
                    <a href="/" class="link--hover inter--small">Learn More</a>
                </li>
                <li>
                    <a href="/faq" class="link--hover inter--small">Need Help?</a>
                </li>
                <li>
                    <a href="/support" class="link--hover inter--small">Contact</a>
                </li>
                <li>
                    <a href="/terms-and-conditions" class="link--hover inter--small">Terms & Conditions</a>
                </li>
                <li>
                    <a href="/rental-agreement" class="link--hover inter--small">Rental Agreement</a>
                </li>
                <li>
                    <a href="/privacy-policy" class="link--hover inter--small">Privacy Policy</a>
                </li>
            </ul>

            <img
                class="db m--auto hide--lg mb2"
                src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"
                width="40%"
            />

            <div class="row hide--lg">
                <div class="col c8 tr">
                    <a href="/" class="link--hover inter--small">Learn More</a>
                </div>
                <div class="col c8">
                    <a href="/faq" class="link--hover inter--small">Need Help?</a>
                </div>
                <div class="col c8 tr">
                    <a href="/support" class="link--hover inter--small">Contact</a>
                </div>
                <div class="col c8">
                    <a href="/terms-conditions" class="link--hover inter--small">Terms & Conditions</a>
                </div>
                <div class="col c8 tr">
                    <a href="/rental-agreement" class="link--hover inter--small">Rental Agreement</a>
                </div>
                <div class="col c8">
                    <a href="/privacy-policy" class="link--hover inter--small">Privacy Policy</a>
                </div>
            </div>
        </div>
    </footer>


</body>
</html>
